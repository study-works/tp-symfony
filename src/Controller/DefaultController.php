<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="app.index")
     */
    public function indexAction(): Response
    {
        return $this->render('base.html.twig', []);
    }

    /**
     * @Route("/hello-{name}", name="app.helloworld")
     */
    public function helloWorldAction(string $name):  Response
    {
        return $this->render('helloworld.html.twig', [
            'name' => $name,
        ]);
    }
}
