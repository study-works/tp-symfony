<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;

class ListPersonsFromFileCommand extends Command
{
    protected static $defaultName = 'display-persons';

    protected function configure()
    {
        $this
            ->setDescription('Affiche une liste de personnes.')
            ->setHelp('Affiche sur la sortie standard une liste de personnes stockée dans un fichier yaml.')
            ->addArgument('file',
                InputArgument::OPTIONAL, 'Le chemin vers le fichier contenant la liste')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $filePath = './var/people.yml';

        $arg1 = $input->getArgument('file');

        if ($arg1) {
            $filePath = $arg1;
        }

        $values = Yaml::parseFile($filePath);

        foreach ($values as $value) {
            if (is_array($value)) {
                foreach ($value as $name) {
                    $output->writeln($name);
                }
            } else {
                $output->writeln($value);
            }
        }

        return 0;
    }
}
