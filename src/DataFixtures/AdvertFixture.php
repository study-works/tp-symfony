<?php

namespace App\DataFixtures;

use App\Entity\Advert;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AdvertFixture extends BaseFixture implements DependentFixtureInterface
{
    private static $advertTitleArray = [
        "A Song of Ice and Fire",
        "The Lord of the Rings",
        "Tintin",
        "Librem 5",
    ];

    private static $advertPhoto1Array = [
        "https://cdn.pixabay.com/photo/2015/12/29/14/51/landscape-1112911_960_720.jpg",
        "https://cdn.pixabay.com/photo/2016/03/18/15/02/ufo-1265186_960_720.jpg",
        "https://cdn.pixabay.com/photo/2014/12/27/16/38/planet-581239_960_720.jpg",
        "https://cdn.pixabay.com/photo/2017/10/17/19/11/fantasy-2861815_960_720.jpg",
        "https://cdn.pixabay.com/photo/2014/05/10/18/10/saturn-341379_960_720.jpg",
        "https://cdn.pixabay.com/photo/2015/08/28/11/27/space-911785_960_720.jpg",
        "https://cdn.pixabay.com/photo/2018/04/20/01/07/science-3334826_960_720.jpg",
        "https://cdn.pixabay.com/photo/2019/03/18/17/29/fantasy-4063619_960_720.jpg",
        "https://cdn.pixabay.com/photo/2016/05/24/12/24/rock-1412287_960_720.jpg",
        "https://cdn.pixabay.com/photo/2014/09/11/12/45/spacecraft-441708_960_720.jpg",
    ];

    protected function loadData(ObjectManager $manager)
    {
        $advertTitleArray = CategoryFixture::ADVERT_TITLE_ARRAY;

        $this->createMany(Advert::class, 20, function (Advert $advert) use ($advertTitleArray) {
            $advert->setTitle($this->faker->randomElement(self::$advertTitleArray))
                ->setContent($this->faker->text(250))
                ->setPrice($this->faker->randomFloat(2, 10, 1500))
                ->setPostalCode(rand(63000, 63850))
                ->setCreationDate($this->faker->dateTimeBetween('-100 days', '-1days'))
                ->setPhoto1($this->faker->randomElement(self::$advertPhoto1Array))
                ->setCategory($this->getReference($advertTitleArray[array_rand($advertTitleArray, 1)]))
            ;
        });

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            CategoryFixture::class,
        ];
    }
}
