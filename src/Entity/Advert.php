<?php

namespace App\Entity;

use App\Repository\AdvertRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="advert")
 * @ORM\Entity(repositoryClass=AdvertRepository::class)
 */
class Advert
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $Title;

    /**
     * @Assert\NotBlank
     *
     * @ORM\Column(name="content", type="text")
     */
    private $Content;

    /**
     * @Assert\NotBlank
     * @Assert\Positive
     *
     * @ORM\Column(name="price", type="float")
     */
    private $Price;

    /**
     * @Assert\NotBlank
     * @Assert\Length(max = 6)
     * @Assert\Positive
     *
     * @ORM\Column(name="postal_code", type="string", length=6)
     */
    private $PostalCode;

    /**
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $CreationDate;

    /**
     * @Assert\NotBlank
     * @Assert\Url
     *
     * @ORM\Column(name="photo1", type="text", nullable=true)
     */
    private $Photo1;

    /**
     * @ORM\Column(name="photo2", type="text", nullable=true)
     */
    private $Photo2;

    /**
     * @ORM\Column(name="photo3", type="text", nullable=true)
     */
    private $Photo3;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="adverts")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $Category;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->Content;
    }

    public function setContent(string $Content): self
    {
        $this->Content = $Content;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->Price;
    }

    public function setPrice(float $Price): self
    {
        $this->Price = $Price;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->PostalCode;
    }

    public function setPostalCode(string $PostalCode): self
    {
        $this->PostalCode = $PostalCode;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->CreationDate;
    }

    public function setCreationDate(\DateTimeInterface $CreationDate): self
    {
        $this->CreationDate = $CreationDate;

        return $this;
    }

    public function getPhoto1(): ?string
    {
        return $this->Photo1;
    }

    public function setPhoto1(?string $Photo1): self
    {
        $this->Photo1 = $Photo1;

        return $this;
    }

    public function getPhoto2(): ?string
    {
        return $this->Photo2;
    }

    public function setPhoto2(?string $Photo2): self
    {
        $this->Photo2 = $Photo2;

        return $this;
    }

    public function getPhoto3(): ?string
    {
        return $this->Photo3;
    }

    public function setPhoto3(?string $Photo3): self
    {
        $this->Photo3 = $Photo3;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->Category;
    }

    public function setCategory(?Category $Category): self
    {
        $this->Category = $Category;

        return $this;
    }
}
